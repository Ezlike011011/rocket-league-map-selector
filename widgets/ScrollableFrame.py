import tkinter as tk
from tkinter import ttk

# Scrollable frame used for flashcardsetviewer and cardList
class ScrollFrame(ttk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.grid_rowconfigure(0, weight = 1)
        self.grid_columnconfigure(0, weight = 10)
        self.grid_columnconfigure(1, weight = 0)

        self.canvas = tk.Canvas(self, borderwidth=0, background="#4a4a4a")          
        self.viewPort = ttk.Frame(self.canvas)                  
        self.vsb = ttk.Scrollbar(self, orient="vertical", command=self.canvas.yview)  
        self.canvas.configure(yscrollcommand=self.vsb.set)                          
     
        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)                                 
        self.canvas_window = self.canvas.create_window((4,4), window=self.viewPort, anchor="nw",
                                  tags="self.viewPort")

        self.viewPort.bind("<Configure>", self.onFrameConfigure)                       
        self.canvas.bind("<Configure>", self.onCanvasConfigure)                       
        self.onFrameConfigure(None)                                                 

    def onFrameConfigure(self, event):                                              
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))                 

    def onCanvasConfigure(self, event):
        '''Reset the canvas window to encompass inner frame when required'''
        canvas_width = event.width
        self.canvas.itemconfig(self.canvas_window, width = canvas_width)            