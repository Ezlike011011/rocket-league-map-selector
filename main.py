import tkinter as tk
from tkinter import ttk
from ttkthemes import ThemedTk
from application import MapSelectorApp

def main():
    root = tk.Tk()
    
    root.grid_rowconfigure(0, weight = 1)
    root.grid_columnconfigure(0, weight = 1)
    root.title("Rocket League Custom Map Manager")
    root.minsize(400, 200)

    app = MapSelectorApp(root)
    app.grid(row = 0, column = 0, sticky = tk.NSEW)

    root.mainloop()

if __name__ == "__main__":
    main()