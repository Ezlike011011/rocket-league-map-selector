# Rocket League Map Selector

This application is a utility tool for selecting maps to host in a custom
rocket league match. For information on how to set up this kind of thing, look
to this tutorial https://www.youtube.com/watch?v=vfIIa2cUZSE