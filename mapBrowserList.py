import tkinter as tk
from tkinter import ttk
from widgets.ScrollableFrame import ScrollFrame


class MapBrowserList(tk.Frame):
    """A scrollable list of buttons that correspond to maps
    """
    def __init__(self, mapList, callback, master = None,):
        super().__init__(master)
        self.buildWidgets()
        self.buttons = []
        self.rebuildMapList(mapList)
        self.callback = callback


    def buildWidgets(self):
        self.grid_rowconfigure(0, weight = 1)
        self.grid_columnconfigure(0, weight = 1)
        
        self.frame = ScrollFrame(self)

        self.frame.grid(row = 0, column = 0, sticky = tk.NSEW)

    def rebuildMapList(self, maplist):
        for button in self.buttons:
            button.pack_forget()
        
        for mapName, mapFile in maplist:
            thisButton = ttk.Button(self.frame.canvas, text = mapName, command = lambda mapfile = mapFile: self.callback(mapfile))

            self.buttons.append(thisButton)
            self.buttons[-1].pack(fill = tk.X)