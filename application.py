import os
import shutil
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from mapBrowserList import MapBrowserList
import shelve
from pathlib import Path
import re

class MapSelectorApp(ttk.Frame):
    """The UI and required logic for running this map selector app
    """
    def __init__(self, master = None):
        super().__init__(master)
        self.master = master
        self.mapName = r"Labs_Underpass_P.upk"

        if(Path("RLMapPickerPrefs.db.dat").exists()):
            with shelve.open("RLMapPickerPrefs.db") as shelf:
                self.previousCurrentMap = shelf["CurrentMap"]
                self.steamDirectory = shelf["SteamDirectory"]
        else:
            self.changesteamDirectory()
            self.previousCurrentMap = ""
            with shelve.open("RLMapPickerPrefs.db") as shelf:
                shelf["CurrentMap"] = self.previousCurrentMap
                shelf["SteamDirectory"] = self.steamDirectory


        self.maps = []
        self.getMapList()
        self.buildWidgets()

    def buildWidgets(self):
        """Build Widgets required for operation
        """

        # Configure grid
        self.grid_rowconfigure(0, weight = 0)
        self.grid_rowconfigure(1, weight = 0)
        self.grid_rowconfigure(2, weight = 8)
        self.grid_rowconfigure(3, weight = 0)
        self.grid_rowconfigure(4, weight = 0)

        self.grid_columnconfigure(0, weight = 1)

        # Build menu bar
        self.menuBar = tk.Menu(self)

        # Make config dropdown
        self.configMenu = tk.Menu(self.menuBar, tearoff = 0)
        self.configMenu.add_command(label = "Change Rocket League Directory", command = self.changesteamDirectory)

        # Add config dropdown
        self.menuBar.add_cascade(label = "Config", menu = self.configMenu)

        # Apply menubar to root
        self.master.config(menu = self.menuBar)

        # Make/Configure search bar "widget"
        self.searchBarFrame = ttk.Frame(self)
        self.searchBarFrame.grid_columnconfigure(0, weight = 4)
        self.searchBarFrame.grid_columnconfigure(1, weight = 1)
        self.searchBarFrame.grid_rowconfigure(0, weight = 1)

        # Build Search Bar
        self.searchQueryVar = tk.StringVar()
        self.searchBar = ttk.Entry(self.searchBarFrame, textvariable = self.searchQueryVar, background = "#4a4a4a", foreground = "#2a2a2a")

        # Make search button
        self.searchButton = ttk.Button(self.searchBarFrame, command = self.refreshMapList, text = "Search")

        # Place Search Bar and Button
        self.searchBar.grid(row = 0, column = 0, sticky = tk.NSEW)
        self.searchButton.grid(row = 0, column = 1, sticky = tk.NSEW)
        
        # Place search bar frame
        self.searchBarFrame.grid(row = 0, column = 0, sticky = tk.NSEW)

        # Add and place divider
        self.topDivider = ttk.Separator(self)
        self.topDivider.grid(row = 1, column = 0, sticky = tk.NSEW)

        # Make map list
        self.mapListWidget = MapBrowserList(self.maps, self.changeMap, self)

        # Grid Map List
        self.mapListWidget.grid(row = 2, column = 0, sticky = tk.NSEW)

        # Add and place divider
        self.bottomDivider = ttk.Separator(self)
        self.bottomDivider.grid(row = 3, column = 0, sticky = tk.NSEW)
    
        # Make and configure current map display widget
        self.currentMapFrame = ttk.Frame()
        self.currentMapFrame.grid_columnconfigure(0, weight = 0)
        self.currentMapFrame.grid_columnconfigure(1, weight = 3)
        self.currentMapFrame.grid_columnconfigure(2, weight = 1)
        self.currentMapFrame.grid_rowconfigure(0, weight = 1)

        # Add "Current Map" section
        self.currentMapLabel = ttk.Label(self.currentMapFrame, text = "Current Map: ")

        self.currentMapString = tk.StringVar(self, self.previousCurrentMap)
        self.currentMapDisplay = ttk.Entry(self.currentMapFrame, state = tk.DISABLED, textvariable = self.currentMapString, background = "#4a4a4a", foreground = "#2a2a2a")

        self.clearCurrentMapButton = ttk.Button(self.currentMapFrame, text = "Clear", command = self.clearCurrentMap)

        # Build current Map display "Widget"
        self.currentMapLabel.grid(row = 0, column = 0, sticky = tk.NSEW)
        self.currentMapDisplay.grid(row = 0, column = 1, sticky = tk.NSEW)
        self.clearCurrentMapButton.grid(row = 0, column = 2, sticky = tk.NSEW)

        self.currentMapFrame.grid(row = 4, column = 0, sticky = tk.NSEW)
     
    
    def changesteamDirectory(self):
        """Creates a Dialog to ask the user to find the rocket league folder
        """

        newDir = ""
        valid = False
        while not valid:
            newDir = filedialog.askdirectory(title = "Select your steamapps folder", mustexist = True)
            
            if(Path(newDir).parts[-1] == "steamapps"):
                valid = True

        self.steamDirectory = newDir

    def refreshMapList(self):
        """Search the known maps based on searchQueryVar and update mapListWidget 
        """

        query = self.searchQueryVar.get()

        maplist = [mapObject for mapObject in self.maps if (query in mapObject[0])]

        self.mapListWidget.rebuildMapList(maplist)

    def clearCurrentMap(self):
        """Clears the current map from the shelf, display and deletes file
        """
        self.currentMapString.set("")
        self.modFolder = Path(self.steamDirectory) / 'common' / 'rocketleague' / 'TAGame' / 'CookedPCConsole' / 'mods'

        os.remove(self.modFolder / self.mapName)

        with shelve.open("RLMapPickerPrefs.db") as shelf:
            shelf["CurrentMap"] = self.currentMapString.get()


    def getMapList(self):
        """Collects maps from known map directory and populates self.mapList 
        """

        self.maps = []
        mapFolder = Path(self.steamDirectory) / 'workshop' / 'Content' / '252950'

        for mapFile in mapFolder.glob("**/*.udk"):
            name = mapFile.stem.replace("_", " ")

            name = re.sub("[A-Z][a-z]*|\d", lambda match: " " + match.group(0), name)

            self.maps.append((name, mapFile))

    def changeMap(self, mapFile):
        """change the currently active map, replacing the file properly
        """

        # Replace map file in rl folder
        self.modFolder = Path(self.steamDirectory) / 'common' / 'rocketleague' / 'TAGame' / 'CookedPCConsole' / 'mods'

        try:
            os.remove(self.modFolder / self.mapName)
        except FileNotFoundError:
            # If the file doesn't exitst already we don't care
            pass

        
        shutil.copy(mapFile, self.modFolder / self.mapName)

        # Replace "Current Map"
        self.currentMapString.set(mapFile.name)

        # Update shelf
        with shelve.open("RLMapPickerPrefs.db") as shelf:
            shelf["CurrentMap"] = self.currentMapString.get()
            shelf["SteamDirectory"] = self.steamDirectory